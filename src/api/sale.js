import { get, post } from "@/utils/request";
/**
 * 获取订单记录
 * @export
 * @param {*} data
 * @returns
 */
export function getOrderList(data) {
  return get("/admin/order", data);
}

/**
 * 修改订单状态
 * @export
 * @param {*} data
 */
export function modifyState(data) {
  return post("/order/update", data);
}

/**
 * 删除订单
 * @export
 * @param {*} data
 * @returns
 */
export function deleleOrder(data) {
  return post("/order/delete", data);
}

/**
 * 取消订单
 * @param { { orderId: number, uid: number } } data
 * @returns
 */
export function cancelOrder(data) {
  return post("/order/cancel", data);
}

/**
 * 导出商品销售记录excel
 * @param {{ start: string, end: string, campus: 1 | 2 }} data
 */
export function getOrderRecordExcel(data) {
  return post("/admin/getOrderRecordExcel", data, {
    responseType: "blob",
    headers: {}
  });
}
